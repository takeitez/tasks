package com.takeiteasy.vip.second;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SecondTask {
    private static final String SPACE_DELIMITER = " ";
    private static final String VOWELS_CHARS = "AaEeUuIiOoYy";

    private final Scanner scanner;

    public SecondTask(Scanner scanner) {
        this.scanner = scanner;
    }

    public void run() {
        firstTask();
        secondTask();
        thirdTask();
        fourthTask();
        fifthTask();
        sixthTask();
    }

    private void firstTask() {
        System.out.println("First task");
        System.out.println("Enter 3 numbers:");

        Integer[] numbers = new Integer[3];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.valueOf(scanner.nextLine());
        }

        System.out.println("Squares sum = " + countSquaresSum(numbers));

        System.out.println();
    }

    private int countSquaresSum(Integer[] numbers) {
        int sum = 0;

        for (Integer number : getTwoLargest(numbers)) {
            sum += Math.pow(number, 2);
        }

        return sum;
    }

    private Integer[] getTwoLargest(Integer[] numbers) {
        Arrays.sort(numbers, Collections.reverseOrder());
        return Arrays.copyOf(numbers, 2);
    }

    private void secondTask() {
        System.out.println("Second task");
        System.out.println("Enter 3 numbers a b c separating them with spaces\nto solve ax^2 + bx + c = 0:");

        double[] roots = solveQuadraticEquation(
                Integer.valueOf(scanner.nextLine()),
                Integer.valueOf(scanner.nextLine()),
                Integer.valueOf(scanner.nextLine())
        );

        if (roots.length > 0) {
            System.out.println("First root = " + roots[0]);
            System.out.println("Second root = " + roots[1]);
        } else {
            System.out.println("No real roots exist.");
        }

        System.out.println();
    }

    private double[] solveQuadraticEquation(int a, int b, int c) {
        double[] roots = new double[2];

        double discriminant = b * b - 4 * a * c;

        if (discriminant < 0)
            return new double[0];

        double squareRootOfD = Math.sqrt(Math.abs(discriminant));

        roots[0] = (-b + squareRootOfD) / (2 * a);
        roots[1] = (-b - squareRootOfD) / (2 * a);

        return roots;
    }

    private void thirdTask() {
        System.out.println("Third task");
        System.out.println("Enter count of strings to enter: ");

        String[] strings = requestStringsInput();

        String smallestString = getSmallestString(strings);

        System.out.println("Smallest string \"" + smallestString + "\"\nhas length = " + smallestString.length());
        System.out.println();
    }

    private String getSmallestString(String[] strings) {
        String smallestString = null;

        for (String string : strings) {

            if (smallestString == null) {
                smallestString = string;
            } else if (string.length() < smallestString.length()) {
                smallestString = string;
            }

        }

        return smallestString;
    }

    private void fourthTask() {
        System.out.println("Fourth task");

        String[] strings = requestStringsInput();

        float averageLength = calcAverageStringsLength(strings);

        System.out.println("Average strings length = " + averageLength);

        printStringsSmallerThan(strings, averageLength);

        System.out.println();
    }

    private float calcAverageStringsLength(String[] strings) {
        int min = -1;
        int max = 0;

        for (String string : strings) {

            if (min < 0)
                min = string.length();

            if (string.length() > max)
                max = string.length();

            if (string.length() < min)
                min = string.length();

        }

        return (min + max) / 2f;
    }

    private void printStringsSmallerThan(String[] strings, float averageLength) {
        System.out.println("Strings with length less than " + averageLength + ":");
        for (String string : strings) {
            if (string.length() < averageLength)
                System.out.println(string);
        }
    }

    private void fifthTask() {
        System.out.println("Fifth task");

        String[] strings = requestStringsInput();
        Arrays.sort(strings);

        System.out.println("Sorted strings:");

        for (String string : strings)
            System.out.println(string);

        System.out.println();
    }

    private void sixthTask() {
        System.out.println("Sixth task");

        String[] sentences = requestStringsInput();

        System.out.println("Enter edge length of words to remove:");

        int edgeLength = Integer.valueOf(scanner.nextLine());

        for (String sentence : sentences) {
            String resultSentence = removeWordsSmallerThan(sentence, edgeLength);
            System.out.println(resultSentence);
        }
    }

    private String removeWordsSmallerThan(String sentence, int edgeLength) {
        StringBuilder sb = new StringBuilder();
        String[] words = sentence.split(SPACE_DELIMITER);

        for (String word : words) {
            if (word.length() == edgeLength && !isVowelChar(word.charAt(0)))
                continue;

            sb.append(word).append(SPACE_DELIMITER);
        }

        return sb.toString().trim();
    }

    private boolean isVowelChar(char ch) {
        return VOWELS_CHARS.contains(String.valueOf(ch));
    }

    private String[] requestStringsInput() {
        System.out.println("Enter count of strings to enter: ");

        int count = Integer.valueOf(scanner.nextLine());

        System.out.println("Enter " + count + " strings:");

        String[] strings = new String[count];

        for (int i = 0; i < count; i++) {
            strings[i] = scanner.nextLine();
        }

        return strings;
    }
}
