package com.takeiteasy.vip.weather.observers;

import com.takeiteasy.vip.weather.subject.Weather;
import com.takeiteasy.vip.weather.utils.TimeUtils;
import com.takeiteasy.vip.weather.subject.WeatherSubject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class WeatherChangesRuObserver extends WeatherChangesObserver {
    private final Map<String, String> localeParamNames;

    public WeatherChangesRuObserver(int updatesInterval, WeatherSubject weatherSubject) {
        super(updatesInterval, weatherSubject);
        localeParamNames = createLocaleMap();
    }

    private Map<String, String> createLocaleMap() {
        Map<String, String> map = new HashMap<>();

        map.put(Weather.PARAM_CITY_NAME, "Город");
        map.put(Weather.PARAM_TEMPERATURE, "Температура");
        map.put(Weather.PARAM_PRESSURE, "Давление");
        map.put(Weather.PARAM_HUMIDITY, "Влажность");

        return map;
    }

    @Override
    void printWeatherParameters(ConcurrentMap<String, String> weatherParameters) {
        System.out.println(TimeUtils.getFormattedCurrentTime() + " - RU");

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : weatherParameters.entrySet()) {
            sb.append(localeParamNames.get(entry.getKey()))
                    .append(DELIMITER)
                    .append(entry.getValue())
                    .append(NEXT_LINE);
        }

        System.out.println(sb.toString());
    }
}
