package com.takeiteasy.vip.weather.observers;

import com.takeiteasy.vip.weather.subject.Weather;
import com.takeiteasy.vip.weather.utils.TimeUtils;
import com.takeiteasy.vip.weather.subject.WeatherSubject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class WeatherChangesUaObserver extends WeatherChangesObserver {
    private final Map<String, String> localeParamNames;

    public WeatherChangesUaObserver(int updatesInterval, WeatherSubject weatherSubject) {
        super(updatesInterval, weatherSubject);
        localeParamNames = createLocaleMap();
    }

    private Map<String, String> createLocaleMap() {
        Map<String, String> map = new HashMap<>();

        map.put(Weather.PARAM_CITY_NAME, "Мiсто");
        map.put(Weather.PARAM_TEMPERATURE, "Температура");
        map.put(Weather.PARAM_PRESSURE, "Тиск");
        map.put(Weather.PARAM_HUMIDITY, "Вологiсть");

        return map;
    }

    @Override
    void printWeatherParameters(ConcurrentMap<String, String> weatherParameters) {
        System.out.println(TimeUtils.getFormattedCurrentTime() + " - UA");

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : weatherParameters.entrySet()) {
            sb.append(localeParamNames.get(entry.getKey()))
                    .append(DELIMITER)
                    .append(entry.getValue())
                    .append(NEXT_LINE);
        }

        System.out.println(sb.toString());
    }
}
