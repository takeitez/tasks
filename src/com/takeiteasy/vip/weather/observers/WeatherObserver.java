package com.takeiteasy.vip.weather.observers;

public interface WeatherObserver {
    void notifyWeatherChanged();
}
