package com.takeiteasy.vip.weather.observers;

import com.takeiteasy.vip.weather.subject.Weather;
import com.takeiteasy.vip.weather.utils.TimeUtils;
import com.takeiteasy.vip.weather.subject.WeatherSubject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class WeatherChangesEngObserver extends WeatherChangesObserver {
    private final Map<String, String> localeParamNames;

    public WeatherChangesEngObserver(int updatesInterval, WeatherSubject weatherSubject) {
        super(updatesInterval, weatherSubject);
        localeParamNames = createLocaleMap();
    }

    private Map<String, String> createLocaleMap() {
        Map<String, String> map = new HashMap<>();

        map.put(Weather.PARAM_CITY_NAME, "City");
        map.put(Weather.PARAM_TEMPERATURE, "Temperature");
        map.put(Weather.PARAM_PRESSURE, "Pressure");
        map.put(Weather.PARAM_HUMIDITY, "Humidity");

        return map;
    }

    @Override
    void printWeatherParameters(ConcurrentMap<String, String> weatherParameters) {
        System.out.println(TimeUtils.getFormattedCurrentTime() + " - ENG");

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : weatherParameters.entrySet()) {
            sb.append(localeParamNames.get(entry.getKey()))
                    .append(DELIMITER)
                    .append(entry.getValue())
                    .append(NEXT_LINE);
        }

        System.out.println(sb.toString());
    }
}
