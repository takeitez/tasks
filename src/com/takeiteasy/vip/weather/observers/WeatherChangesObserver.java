package com.takeiteasy.vip.weather.observers;

import com.takeiteasy.vip.weather.subject.WeatherSubject;

import java.util.concurrent.ConcurrentMap;

public abstract class WeatherChangesObserver extends Thread implements WeatherObserver {
    protected static final String DELIMITER = ": ";
    protected static final String NEXT_LINE = "\n";

    private final int updatesInterval;
    private final WeatherSubject weatherSubject;

    private boolean isWeatherChanged;
    private boolean isInterrupted;

    protected WeatherChangesObserver(int updatesInterval, WeatherSubject weatherSubject) {
        this.updatesInterval = updatesInterval;
        this.weatherSubject = weatherSubject;
    }

    @Override
    public void run() {
        while (!isInterrupted) {
            if (isWeatherChanged) {
                printWeatherParameters(weatherSubject.getWeatherParameters());
                isWeatherChanged = false;
            }

            try {
                sleep(updatesInterval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void start() {
        super.start();
        weatherSubject.subscribe(this);
        isInterrupted = false;
    }

    @Override
    public void interrupt() {
        super.interrupt();
        weatherSubject.unsubscribe(this);
        isInterrupted = true;
    }

    abstract void printWeatherParameters(ConcurrentMap<String, String> weatherParameters);

    @Override
    public void notifyWeatherChanged() {
        isWeatherChanged = true;
    }
}
