package com.takeiteasy.vip.weather.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    public static String getFormattedCurrentTime() {
        return TIME_FORMAT.format(new Date(System.currentTimeMillis()));
    }
}
