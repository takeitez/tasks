package com.takeiteasy.vip.weather.subject;

import com.takeiteasy.vip.weather.observers.WeatherObserver;

import java.util.concurrent.ConcurrentMap;

public interface WeatherSubject {
    void subscribe(WeatherObserver observer);
    void unsubscribe(WeatherObserver observer);
    ConcurrentMap<String, String> getWeatherParameters();
}
