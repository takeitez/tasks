package com.takeiteasy.vip.weather.subject;

import com.takeiteasy.vip.weather.utils.TimeUtils;
import com.takeiteasy.vip.weather.observers.WeatherObserver;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Weather extends Thread implements WeatherSubject {
    private static final int UPDATES_INTERVAL_IN_MILLIS = 5000;

    public static final String PARAM_CITY_NAME = "CITY_NAME";
    public static final String PARAM_TEMPERATURE = "TEMPERATURE";
    public static final String PARAM_PRESSURE = "PRESSURE";
    public static final String PARAM_HUMIDITY = "HUMIDITY";

    private final ConcurrentHashMap<String, String> weatherParameters;
    private final Random random;
    private final List<WeatherObserver> weatherObservers;
    private boolean isInterrupted;

    public Weather() {
        weatherParameters = createWeatherParameters();
        random = new Random();
        weatherObservers = new ArrayList<>();
    }

    private ConcurrentHashMap<String, String> createWeatherParameters() {
        ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();

        map.put(PARAM_CITY_NAME, "Kharkiv");
        map.put(PARAM_TEMPERATURE, "4");
        map.put(PARAM_PRESSURE, "750");
        map.put(PARAM_HUMIDITY, "95");

        return map;
    }

    private void setParam(String param, String value) {
        weatherParameters.put(param, value);
    }

    @Override
    public void run() {
        while (!isInterrupted) {
            updateParams();

            try {
                sleep(UPDATES_INTERVAL_IN_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public synchronized void start() {
        super.start();
        isInterrupted = false;
    }

    @Override
    public void interrupt() {
        super.interrupt();
        isInterrupted = true;
    }

    private void updateParams() {
        setParam(PARAM_TEMPERATURE, String.valueOf(random.nextInt(20)));
        setParam(PARAM_PRESSURE, String.valueOf(random.nextInt(800)));
        setParam(PARAM_HUMIDITY, String.valueOf(random.nextInt(100)));
        System.out.println(TimeUtils.getFormattedCurrentTime() + " weather parameters were updated\n ");
        notifyObservers();
    }

    private void notifyObservers() {
        for (WeatherObserver observer : weatherObservers) {
            observer.notifyWeatherChanged();
        }
    }

    @Override
    public void subscribe(WeatherObserver observer) {
        weatherObservers.add(observer);
    }

    @Override
    public void unsubscribe(WeatherObserver observer) {
        weatherObservers.remove(observer);
    }

    @Override
    public ConcurrentMap<String, String> getWeatherParameters() {
        return weatherParameters;
    }
}
