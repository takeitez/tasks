package com.takeiteasy.vip.weather;

import com.takeiteasy.vip.weather.observers.WeatherChangesEngObserver;
import com.takeiteasy.vip.weather.observers.WeatherChangesObserver;
import com.takeiteasy.vip.weather.observers.WeatherChangesRuObserver;
import com.takeiteasy.vip.weather.observers.WeatherChangesUaObserver;
import com.takeiteasy.vip.weather.subject.Weather;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MeteoService {
    private static final String COMMAND_EXIT = "exit";

    private final Scanner scanner;
    private final Weather weather;
    private final List<WeatherChangesObserver> weatherChangesObservers;

    public MeteoService(Scanner scanner) {
        this.scanner = scanner;
        weather = new Weather();
        weatherChangesObservers = createWeatherChangesObservers();
    }

    private List<WeatherChangesObserver> createWeatherChangesObservers() {
        List<WeatherChangesObserver> observers = new ArrayList<>();

        observers.add(new WeatherChangesEngObserver(7000, weather));
        observers.add(new WeatherChangesRuObserver(3000, weather));
        observers.add(new WeatherChangesUaObserver(1000, weather));

        return observers;
    }

    public void run() {
        System.out.println("Enter 'exit' to abort updates.");
        weather.start();
        enableObservers();

        boolean stop = false;

        while (!stop) {
            String command = scanner.nextLine();

            if (command.equalsIgnoreCase(COMMAND_EXIT)) {
                weather.interrupt();
                disableObservers();
                stop = true;
            }
        }
    }

    private void enableObservers() {
        for (WeatherChangesObserver observer : weatherChangesObservers) {
            observer.start();
        }
    }

    private void disableObservers() {
        for (WeatherChangesObserver observer : weatherChangesObservers) {
            observer.interrupt();
        }
    }
}
