package com.takeiteasy.vip.fifth.library;

import com.takeiteasy.vip.fifth.library.models.Book;
import com.takeiteasy.vip.fifth.library.models.LibraryItem;
import com.takeiteasy.vip.fifth.library.models.Magazine;
import com.takeiteasy.vip.fifth.library.models.Yearbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {
    /**
     * Assume that we get this value in an appropriate way.
     */
    private static final int CURRENT_YEAR = 2017;

    private final List<LibraryItem> libraryItems;
    private final Scanner scanner;

    public Library(Scanner scanner) {
        this.scanner = scanner;
        libraryItems = createLibraryItemsList();
    }

    public void run() {
        System.out.println("Enter number of years to filter\n(relative to current, 0 - means all items for current year): ");
        printItemsWithDateGreaterOrEqual(CURRENT_YEAR - Integer.valueOf(scanner.nextLine()));
    }

    private List<LibraryItem> createLibraryItemsList() {
        List<LibraryItem> list = new ArrayList<>();
        list.add(new Book("Book 1", 2015, "Author 1", "Publisher 1"));
        list.add(new Magazine("Magazine 1", 2013, "Subject 1"));
        list.add(new Yearbook("Yearbook 1", 2014, "Subject 1", "Publisher 1"));
        list.add(new Book("Book 2", 2017, "Author 2", "Publisher 2"));
        list.add(new Magazine("Magazine 2", 2016, "Subject 2"));
        list.add(new Yearbook("Yearbook 2", 2017, "Subject 2", "Publisher 2"));
        return list;
    }

    private void printItemsWithDateGreaterOrEqual(int date) {
        for (LibraryItem item : libraryItems) {
            if (item.isDateGreaterOrEqual(date)) {
                item.printInfo();
            }
        }
    }
}
