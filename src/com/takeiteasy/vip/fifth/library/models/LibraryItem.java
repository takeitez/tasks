package com.takeiteasy.vip.fifth.library.models;

public abstract class LibraryItem {
    final String name;

    /**
     * Assume date is integer number (1890, etc).
     */
    final int date;

    LibraryItem(String name, int date) {
        this.name = name;
        this.date = date;
    }

    public boolean isDateGreaterOrEqual(int date) {
        return this.date >= date;
    }

    public abstract void printInfo();
}
