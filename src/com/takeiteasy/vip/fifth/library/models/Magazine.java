package com.takeiteasy.vip.fifth.library.models;

public class Magazine extends LibraryItem {
    private final String subject;

    public Magazine(String name, int date, String subject) {
        super(name, date);
        this.subject = subject;
    }

    @Override
    public void printInfo() {
        System.out.println("Book: Name='" + name + "', Subject='" + subject + "', date='" + date + "'.");
    }
}
