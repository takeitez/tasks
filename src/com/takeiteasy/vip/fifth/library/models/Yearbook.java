package com.takeiteasy.vip.fifth.library.models;

public class Yearbook extends LibraryItem {
    private final String subject;
    private final String publisher;

    public Yearbook(String name, int date, String subject, String publisher) {
        super(name, date);
        this.subject = subject;
        this.publisher = publisher;
    }

    @Override
    public void printInfo() {
        System.out.println("Book: Name='" + name + "', Subject='" + subject + "', Publisher='" + publisher + "', date='" + date + "'.");
    }
}
