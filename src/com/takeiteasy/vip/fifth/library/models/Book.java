package com.takeiteasy.vip.fifth.library.models;

public class Book extends LibraryItem {
    private final String author;
    private final String publisher;

    public Book(String name, int date, String author, String publisher) {
        super(name, date);
        this.author = author;
        this.publisher = publisher;
    }

    @Override
    public void printInfo() {
        System.out.println("Book: Name='" + name + "', Author='" + author + "', Publisher='" + publisher + "', date='" + date + "'.");
    }
}
