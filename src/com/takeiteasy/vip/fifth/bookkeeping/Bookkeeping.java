package com.takeiteasy.vip.fifth.bookkeeping;

import com.takeiteasy.vip.fifth.bookkeeping.models.*;

import java.util.ArrayList;
import java.util.List;

public class Bookkeeping {
    private final List<Employee> employees;

    public Bookkeeping() {
        employees = createEmployeesList();
    }

    public void run() {
        printEmployeesSalary();
    }

    private List<Employee> createEmployeesList() {
        List<Employee> list = new ArrayList<>();
        list.add(new MonthRateEmployee("Ivanova Elena", "zam Directora", 7500));
        list.add(new HoursRateEmployee("Vakulenko Dmitriy", "Designer", 7));
        list.add(new SalesRateEmployee("Koren'kova Anna", "Sales manager", 0.05f, new YearSales(50000, 65000)));
        list.add(new MixedMonthSalesRateEmployee("Tatiana Sergeevna", "Sales manager", 1000, 0.03f, new YearSales(50000, 65000)));
        return list;
    }

    private void printEmployeesSalary() {
        for (Employee employee : employees) {
            printEmployeeSalary(employee);
        }
    }

    private void printEmployeeSalary(Employee employee) {
        System.out.println("Name='" + employee.getFullName()
                + "', Position='" + employee.getPosition()
                + "', Month income='" + employee.calcMonthSalary()
                + "', Year income='" + employee.calcYearSalary()
                + "'.");
    }
}
