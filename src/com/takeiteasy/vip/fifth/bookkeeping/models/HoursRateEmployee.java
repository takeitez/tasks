package com.takeiteasy.vip.fifth.bookkeeping.models;

public class HoursRateEmployee extends Employee {

    private final float hourRate;

    public HoursRateEmployee(String fullName, String position, float hourRate) {
        super(fullName, position);
        this.hourRate = hourRate;
    }

    @Override
    public float calcMonthSalary() {
        return hourRate * Constants.USD_RATE * Constants.WORKING_HOURS_PER_MONTH_COUNT;
    }

    @Override
    public float calcYearSalary() {
        return calcMonthSalary() * Constants.MONTHS_COUNT;
    }
}
