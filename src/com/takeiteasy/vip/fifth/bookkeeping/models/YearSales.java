package com.takeiteasy.vip.fifth.bookkeeping.models;

public class YearSales {
    private final float firstHalfOfYEar;
    private final float lastHalfOfYear;

    public YearSales(float firstHalfOfYEar, float lastHalfOfYear) {
        this.firstHalfOfYEar = firstHalfOfYEar;
        this.lastHalfOfYear = lastHalfOfYear;
    }

    float getAveragePerMonthAmount() {
        return getYearSalesAmount() / Constants.MONTHS_COUNT;
    }

    float getYearSalesAmount() {
        return firstHalfOfYEar + lastHalfOfYear;
    }
}
