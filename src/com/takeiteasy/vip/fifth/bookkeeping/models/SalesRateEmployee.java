package com.takeiteasy.vip.fifth.bookkeeping.models;

public class SalesRateEmployee extends Employee {
    private final float salesRate;
    private final YearSales yearSales;

    public SalesRateEmployee(String fullName, String position, float salesRate, YearSales yearSales) {
        super(fullName, position);
        this.salesRate = salesRate;
        this.yearSales = yearSales;
    }

    @Override
    public float calcMonthSalary() {
        return yearSales.getAveragePerMonthAmount() * salesRate;
    }

    @Override
    public float calcYearSalary() {
        return yearSales.getYearSalesAmount() * salesRate;
    }
}
