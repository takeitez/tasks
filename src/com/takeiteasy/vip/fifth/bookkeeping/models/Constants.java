package com.takeiteasy.vip.fifth.bookkeeping.models;

public interface Constants {
    int MONTHS_COUNT = 12;
    int WORKING_HOURS_PER_MONTH_COUNT = 160;
    float USD_RATE = 27.2f;
}
