package com.takeiteasy.vip.fifth.bookkeeping.models;

public class MixedMonthSalesRateEmployee extends SalesRateEmployee {
    private final float monthRate;

    public MixedMonthSalesRateEmployee(String fullName, String position, float monthRate, float salesRate, YearSales yearSales) {
        super(fullName, position, salesRate, yearSales);
        this.monthRate = monthRate;
    }

    @Override
    public float calcMonthSalary() {
        return monthRate + super.calcMonthSalary();
    }

    @Override
    public float calcYearSalary() {
        return monthRate * Constants.MONTHS_COUNT + super.calcYearSalary();
    }
}
