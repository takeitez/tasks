package com.takeiteasy.vip.fifth.bookkeeping.models;

public abstract class Employee {
    private final String fullName;
    private final String position;

    Employee(String fullName, String position) {
        this.fullName = fullName;
        this.position = position;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPosition() {
        return position;
    }

    public abstract float calcMonthSalary();

    public abstract float calcYearSalary();
}
