package com.takeiteasy.vip.fifth.bookkeeping.models;

public class MonthRateEmployee extends Employee {

    private final float monthRate;

    public MonthRateEmployee(String fullName, String position, float monthRate) {
        super(fullName, position);
        this.monthRate = monthRate;
    }

    @Override
    public float calcMonthSalary() {
        return monthRate;
    }

    @Override
    public float calcYearSalary() {
        return monthRate * Constants.MONTHS_COUNT;
    }
}
