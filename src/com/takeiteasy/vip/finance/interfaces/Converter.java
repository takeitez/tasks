package com.takeiteasy.vip.finance.interfaces;

public interface Converter {
    float intoUAH(float amount);
    float fromUAH(float amount);
}
