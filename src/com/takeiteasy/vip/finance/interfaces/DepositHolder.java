package com.takeiteasy.vip.finance.interfaces;

public interface DepositHolder {
    int EDGE_MONTH_COUNT = 12;
    void deposit(int amount, int monthsCount);
    boolean isSuitableFor(int monthsCount);
}
