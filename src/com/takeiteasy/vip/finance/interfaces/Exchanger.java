package com.takeiteasy.vip.finance.interfaces;

import com.takeiteasy.vip.finance.models.Currency;

public interface Exchanger {
    int EXCHANGE_LIMIT_EXCEEDED = -1;

    Currency findCurrencyByName(String currencyName);
    float convert(float amountInUAH, Currency resultCurrency);
    float convertIntoUAH(float amount, Currency originCurrency);
}
