package com.takeiteasy.vip.finance.interfaces;

public interface Sender {
    void sendMoneyTo(int amount, String recipient);
    float calcFee(int amount);
}
