package com.takeiteasy.vip.finance;

import com.takeiteasy.vip.finance.interfaces.DepositHolder;
import com.takeiteasy.vip.finance.interfaces.Exchanger;
import com.takeiteasy.vip.finance.interfaces.Sender;
import com.takeiteasy.vip.finance.models.*;

import java.util.ArrayList;
import java.util.List;

public class Finance {
    private final List<Currency> currencies;
    private final List<FinanceOrganization> organizations;

    public Finance() {
        currencies = createCurrenciesList();
        organizations = createOrganizations();
    }

    public void run() {
        findBestExchangerForEachCurrency(20_000, getAllExchangers());
        System.out.println();
        findBestLender(50_000, getAllLenders());
        System.out.println();
        findBestDepositHolder(12, getAllDepositHolders());
        System.out.println();
        findBestSender(1000, getAllSenders());
    }

    private void findBestSender(int amountInUAH, List<Sender> senders) {
        System.out.println("Looking for best sender...");

        if (senders.isEmpty()) {
            System.out.println("No best sender for " + amountInUAH + " hrn was found.");
            return;
        }

        Sender best = senders.get(0);
        float fewestFee = best.calcFee(amountInUAH);

        for (int i = 1; i < senders.size(); i++) {
            float fee = senders.get(i).calcFee(amountInUAH);

            if (fee < fewestFee) {
                fewestFee = fee;
                best = senders.get(i);
            }
        }

        System.out.println("Best sender for " + amountInUAH + " hrn is " + best + ".");
    }

    private List<Sender> getAllSenders() {
        List<Sender> list = new ArrayList<>();

        for (FinanceOrganization organization : organizations) {
            if (organization instanceof Sender) {
                list.add((Sender) organization);
            }
        }

        return list;
    }

    private void findBestDepositHolder(int monthsCount, List<DepositHolder> depositHolders) {
        System.out.println("Looking for best deposit holder...");

        DepositHolder best = null;

        for (DepositHolder depositHolder : depositHolders) {
            if (depositHolder.isSuitableFor(monthsCount)) {
                best = depositHolder;
            }
        }

        if (best != null) {
            System.out.println("Best deposit holder for " + monthsCount + " months term is " + best + ".");
        } else {
            System.out.println("No best deposit holder for " + monthsCount + " months term was found.");
        }
    }

    private List<DepositHolder> getAllDepositHolders() {
        List<DepositHolder> list = new ArrayList<>();

        for (FinanceOrganization organization : organizations) {
            if (organization instanceof DepositHolder) {
                list.add((DepositHolder) organization);
            }
        }

        return list;
    }

    private void findBestLender(int amountInUAH, List<Lender> lenders) {
        System.out.println("Looking for best lender...");
        if (lenders.isEmpty()) {
            System.out.println("No best lender for " + amountInUAH + " hrn was found.");
            return;
        }

        Lender best = lenders.get(0);
        float fewestDebt = best.calcDebtAmount(amountInUAH);

        for (int i = 1; i < lenders.size(); i++) {
            float debt = lenders.get(i).calcDebtAmount(amountInUAH);

            if (debt != Lender.LOAN_LIMIT_EXCEEDED && debt < fewestDebt || fewestDebt == Lender.LOAN_LIMIT_EXCEEDED) {
                fewestDebt = debt;
                best = lenders.get(i);
            }
        }

        System.out.println("Best lender for " + amountInUAH + " hrn is " + best + ".");
    }

    private List<Lender> getAllLenders() {
        List<Lender> list = new ArrayList<>();

        for (FinanceOrganization organization : organizations) {
            if (organization instanceof Lender) {
                list.add((Lender) organization);
            }
        }

        return list;
    }

    private void findBestExchangerForEachCurrency(int amountInUAH, List<Exchanger> exchangers) {
        System.out.println("Looking for best exchanger...");

        for (Currency currency : currencies) {
            Exchanger exchanger = findBestExchanger(amountInUAH, currency, exchangers);

            if (exchanger != null) {
                System.out.println("Best exchanger for " + currency.getName() + " is " + exchanger + ".");
            } else {
                System.out.println("No best exchanger for " + currency.getName() + " was found.");
            }
        }
    }

    private Exchanger findBestExchanger(int amountInUAH, Currency currency, List<Exchanger> exchangers) {
        Exchanger best = null;
        float largestExchangeAmount = 0;

        for (Exchanger exchanger : exchangers) {
            float exchangeAmount = exchanger.convert(amountInUAH, currency);

            if (exchangeAmount != Exchanger.EXCHANGE_LIMIT_EXCEEDED && exchangeAmount > largestExchangeAmount) {
                largestExchangeAmount = exchangeAmount;
                best = exchanger;
            }
        }

        return best;
    }

    private List<Exchanger> getAllExchangers() {
        List<Exchanger> list = new ArrayList<>();

        for (FinanceOrganization organization : organizations) {
            if (organization instanceof Exchanger) {
                list.add((Exchanger) organization);
            }
        }

        return list;
    }

    private List<FinanceOrganization> createOrganizations() {
        List<FinanceOrganization> list = new ArrayList<>();
        list.add(new Post("Post", "Post address", 0.02f));
        list.add(new Pawnshop("PawnShop", "PawnShop address", 50_000, 0.4f));
        list.add(new CreditCafe("CreditCafe", "CreditCafe address", 4_000, 2.0f));
        list.add(new CreditUnion("CreditUnion", "CreditUnion address", 100_000, 0.2f));
        list.add(new OpenEndFund("OpenEndFund", "OpenEndFund address"));
        list.add(new Exchange("Exchange", "Exchange address", currencies));
        list.add(new Bank("Bank", "Bank address", 200_000, 0.25f, currencies));
        return list;
    }

    private List<Currency> createCurrenciesList() {
        List<Currency> list = new ArrayList<>();
        list.add(new Currency("USD", 27.2f));
        list.add(new Currency("EUR", 32.1f));
        list.add(new Currency("RUB", 0.48f));
        return list;
    }
}
