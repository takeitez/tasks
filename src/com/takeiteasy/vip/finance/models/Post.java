package com.takeiteasy.vip.finance.models;

import com.takeiteasy.vip.finance.interfaces.Sender;

public class Post extends FinanceOrganization implements Sender {
    private final float sendMoneyFee;

    public Post(String name, String address, float sendMoneyFee) {
        super(name, address);
        this.sendMoneyFee = sendMoneyFee;
    }

    @Override
    public void sendMoneyTo(int amount, String recipient) {
        System.out.println("Post: " + amount + " hrn were sent to " + recipient);
    }

    @Override
    public float calcFee(int amount) {
        return amount * sendMoneyFee;
    }

    @Override
    public String toString() {
        return "Post{" +
                super.toString() +
                ", sendMoneyFee=" + sendMoneyFee +
                "}";
    }
}
