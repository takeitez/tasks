package com.takeiteasy.vip.finance.models;

public class Pawnshop extends Lender {

    public Pawnshop(String name, String address, int loanLimit, float loanRate) {
        super(name, address, loanLimit, loanRate);
    }

    @Override
    public void lend(int amountInUAH) {
        if (amountInUAH <= loanLimit)
            System.out.println(Pawnshop.class.getSimpleName() + ": lent " + amountInUAH + " hrn, debt is " + calcDebtAmount(amountInUAH) + " hrn.");
        else
            System.out.println(Pawnshop.class.getSimpleName() + ": loan limit exceeded, loan limit is " + loanLimit + " hrn.");
    }

    @Override
    public String toString() {
        return "Pawnshop{" + super.toString() + "}";
    }
}
