package com.takeiteasy.vip.finance.models;

public abstract class Lender extends FinanceOrganization {
    public static final int LOAN_LIMIT_EXCEEDED = -1;

    protected int loanLimit;
    private float loanRate;

    Lender(String name, String address, int loanLimit, float loanRate) {
        super(name, address);
        this.loanLimit = loanLimit;
        this.loanRate = loanRate;
    }

    public abstract void lend(int amountInUAH);

    public float calcDebtAmount(int amountInUAH) {
        if (amountInUAH <= loanLimit)
            return amountInUAH + amountInUAH * loanRate;
        else
            return LOAN_LIMIT_EXCEEDED;
    }

    @Override
    public String toString() {
        return super.toString() +
                ", loanLimit=" + loanLimit +
                ", loanRate=" + loanRate;
    }
}
