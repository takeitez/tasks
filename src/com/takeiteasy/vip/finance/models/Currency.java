package com.takeiteasy.vip.finance.models;

import com.takeiteasy.vip.finance.interfaces.Converter;

public class Currency implements Converter {
    private String name;
    private float rate;

    public Currency(String name, float rate) {
        this.name = name;
        this.rate = rate;
    }

    boolean isMatchName(String name) {
        return this.name.equalsIgnoreCase(name);
    }

    @Override
    public float intoUAH(float amount) {
        return amount * rate;
    }

    @Override
    public float fromUAH(float amount) {
        return amount / rate;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "name='" + name + '\'' +
                ", rate=" + rate +
                '}';
    }
}
