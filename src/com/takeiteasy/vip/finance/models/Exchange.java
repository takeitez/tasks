package com.takeiteasy.vip.finance.models;

import com.takeiteasy.vip.finance.interfaces.Exchanger;

import java.util.List;

public class Exchange extends FinanceOrganization implements Exchanger {

    private final List<Currency> supportedCurrencies;

    public Exchange(String name, String address, List<Currency> supportedCurrencies) {
        super(name, address);
        this.supportedCurrencies = supportedCurrencies;
    }

    @Override
    public Currency findCurrencyByName(String currencyName) {
        for (Currency currency : supportedCurrencies) {
            if (currency.isMatchName(currencyName)) {
                return currency;
            }
        }

        return null;
    }

    @Override
    public float convert(float amountInUAH, Currency resultCurrency) {
        return resultCurrency.fromUAH(amountInUAH);
    }

    @Override
    public float convertIntoUAH(float amount, Currency originCurrency) {
        return originCurrency.intoUAH(amount);
    }

    @Override
    public String toString() {
        return "Exchange{" + super.toString() +
                ", supportedCurrencies=" + supportedCurrencies +
                "} ";
    }
}
