package com.takeiteasy.vip.finance.models;

public class FinanceOrganization {
    private final String name;
    private final String address;

    public FinanceOrganization(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", address='" + address + '\'';
    }
}
