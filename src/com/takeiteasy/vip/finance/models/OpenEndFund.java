package com.takeiteasy.vip.finance.models;

import com.takeiteasy.vip.finance.interfaces.DepositHolder;

public class OpenEndFund extends FinanceOrganization implements DepositHolder {

    public OpenEndFund(String name, String address) {
        super(name, address);
    }

    @Override
    public void deposit(int amount, int monthsCount) {
        if (isSuitableFor(monthsCount))
            System.out.println("OpenEndFund: accept " + amount + " deposit for " + monthsCount + " months.");
        else
            System.out.println("OpenEndFund: can't accept deposit in term less then " + EDGE_MONTH_COUNT + " months.");
    }

    @Override
    public boolean isSuitableFor(int monthsCount) {
        return monthsCount >= EDGE_MONTH_COUNT;
    }

    @Override
    public String toString() {
        return "OpenEndFund{" + super.toString() + "}";
    }
}
