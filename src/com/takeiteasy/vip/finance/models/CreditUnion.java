package com.takeiteasy.vip.finance.models;

public class CreditUnion extends Lender {

    public CreditUnion(String name, String address, int loanLimit, float loanRate) {
        super(name, address, loanLimit, loanRate);
    }

    @Override
    public void lend(int amountInUAH) {
        if (amountInUAH <= loanLimit)
            System.out.println(CreditUnion.class.getSimpleName() + ": lent " + amountInUAH + " hrn, debt is " + calcDebtAmount(amountInUAH) + " hrn.");
        else
            System.out.println(CreditUnion.class.getSimpleName() + ": loan limit exceeded, loan limit is " + loanLimit + " hrn.");
    }

    @Override
    public String toString() {
        return "CreditUnion{" + super.toString() + "}";
    }
}
