package com.takeiteasy.vip.finance.models;

import com.takeiteasy.vip.finance.interfaces.DepositHolder;
import com.takeiteasy.vip.finance.interfaces.Exchanger;
import com.takeiteasy.vip.finance.interfaces.Sender;

import java.util.List;

public class Bank extends Lender implements Exchanger, Sender, DepositHolder {
    private static final int EXCHANGE_LIMIT_IN_UAH = 12000;
    private static final int EXCHANGE_FEE_IN_UAH = 15;
    private static final float MONEY_SEND_RATE = 0.01f;
    private static final float MONEY_SEND_EXTRA_RATE = 5;

    private List<Currency> supportedCurrencies;

    public Bank(String name, String address, int loanLimit, float loanRate, List<Currency> supportedCurrencies) {
        super(name, address, loanLimit, loanRate);
        this.supportedCurrencies = supportedCurrencies;
    }

    @Override
    public Currency findCurrencyByName(String currencyName) {
        for (Currency currency : supportedCurrencies) {
            if (currency.isMatchName(currencyName)) {
                return currency;
            }
        }

        return null;
    }

    @Override
    public float convert(float amountInUAH, Currency resultCurrency) {
        if (amountInUAH <= EXCHANGE_LIMIT_IN_UAH)
            return resultCurrency.fromUAH(amountInUAH - EXCHANGE_FEE_IN_UAH);

        return EXCHANGE_LIMIT_EXCEEDED;
    }

    @Override
    public float convertIntoUAH(float amount, Currency originCurrency) {
        float result = originCurrency.intoUAH(amount);

        if (result <= EXCHANGE_LIMIT_IN_UAH)
            return originCurrency.intoUAH(amount) - EXCHANGE_FEE_IN_UAH;

        return EXCHANGE_LIMIT_EXCEEDED;
    }

    @Override
    public void sendMoneyTo(int amount, String recipient) {
        System.out.println(Bank.class.getSimpleName() + ": " + amount + " hrn were sent to " + recipient);
        System.out.println("fee = " + calcFee(amount));
    }

    @Override
    public float calcFee(int amount) {
        return amount * MONEY_SEND_RATE + MONEY_SEND_EXTRA_RATE;
    }

    @Override
    public void deposit(int amount, int monthsCount) {
        if (isSuitableFor(monthsCount))
            System.out.println(Bank.class.getSimpleName() + ": accept " + amount + " deposit for " + monthsCount + " months.");
        else
            System.out.println(Bank.class.getSimpleName() + ": can't accept deposit in term more then " + EDGE_MONTH_COUNT + " months.");
    }

    @Override
    public boolean isSuitableFor(int monthsCount) {
        return monthsCount <= EDGE_MONTH_COUNT;
    }

    @Override
    public void lend(int amountInUAH) {
        if (amountInUAH <= loanLimit)
            System.out.println(Bank.class.getSimpleName() + ": lent " + amountInUAH + " hrn, debt is " + calcDebtAmount(amountInUAH) + " hrn.");
        else
            System.out.println(Bank.class.getSimpleName() + ": loan limit exceeded, loan limit is " + loanLimit + " hrn.");
    }

    @Override
    public String toString() {
        return "Bank{" +
                super.toString() +
                ", exchangeLimit=" + EXCHANGE_LIMIT_IN_UAH +
                ", exchangeFee=" + EXCHANGE_FEE_IN_UAH +
                ", sendRate=" + MONEY_SEND_RATE +
                ", sandExtraRate=" + MONEY_SEND_EXTRA_RATE +
                ", supportedCurrencies=" + supportedCurrencies +
                "} ";
    }
}
