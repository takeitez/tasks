package com.takeiteasy.vip.cafe;

import java.util.*;

public class Cafe {
    private static final String YES = "y";
    private static final String NO = "n";

    private final Scanner scanner;
    private final List<Meal> menu;
    private final Map<Meal, Integer> receipt;

    public Cafe(Scanner scanner) {
        this.scanner = scanner;
        menu = createMenu();
        receipt = new HashMap<>();
    }

    private List<Meal> createMenu() {
        List<Meal> list = new ArrayList<>();

        list.add(new Meal("Meal 1", 15));
        list.add(new Meal("Meal 2", 50));
        list.add(new Meal("Meal 3", 70));
        list.add(new Meal("Meal 4", 10));
        list.add(new Meal("Meal 5", 15));
        list.add(new Meal("Meal 6", 15));

        return list;
    }

    public void run() {
        System.out.println("Menu:");
        printMenu();
        askForOrder();
        System.out.println("Enter number of a meal");
    }

    private void askForOrder() {
        System.out.println("Make an order? (Y/N)");

        if (checkYesNoAnswer()) {
            makeOrder();
        }
    }

    private boolean checkYesNoAnswer() {
        return scanner.nextLine().equalsIgnoreCase(YES);
    }

    private void makeOrder() {
        boolean done = false;

        while (!done) {
            int mealNumber = requestMealNumber();

            if (!isValidMealNumber(mealNumber)) {
                System.out.println("Invalid meal number.");
                continue;
            }

            int amount = requestMealAmount();

            if (!isValidAmount(amount)) {
                System.out.println("Invalid amount value.");
                continue;
            }

            addItemToOrder(menu.get(mealNumber - 1), amount);

            done = requestReceipt();
        }

        printReceipt();
    }

    private void addItemToOrder(Meal meal, int amount) {
        int prevAmount = 0;

        if (receipt.containsKey(meal)) {
            prevAmount = receipt.get(meal);
        }

        receipt.put(meal, prevAmount + amount);
    }

    private void printReceipt() {
        System.out.println("You have ordered:");
        System.out.println("------------");

        int total = 0;

        for (Map.Entry<Meal, Integer> item : receipt.entrySet()) {
            System.out.println(item.getKey() + " - x" + item.getValue());
            total += item.getKey().getPrice() * item.getValue();
        }

        System.out.println("------------");
        System.out.println("Total: " + total);
    }

    private boolean requestReceipt() {
        System.out.println("Keep ordering? (Y/N)");
        return !checkYesNoAnswer();
    }

    private boolean isValidAmount(int amount) {
        return amount > 0;
    }

    private int requestMealAmount() {
        System.out.println("Enter amount:");
        return Integer.valueOf(scanner.nextLine());
    }

    private int requestMealNumber() {
        System.out.println("Enter meal number: ");
        try {
            return Integer.valueOf(scanner.nextLine());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return -1;
    }

    private boolean isValidMealNumber(int number) {
        return number > 0 && number <= menu.size();
    }

    private void printMenu() {
        for (int i = 0; i < menu.size(); i++) {
            System.out.println(i + 1 + ": " + menu.get(i));
        }
    }
}
