package com.takeiteasy.vip;

import com.takeiteasy.vip.cafe.Cafe;
import com.takeiteasy.vip.converter.CurrencyConverter;
import com.takeiteasy.vip.fifth.bookkeeping.Bookkeeping;
import com.takeiteasy.vip.fifth.library.Library;
import com.takeiteasy.vip.finance.Finance;
import com.takeiteasy.vip.second.SecondTask;
import com.takeiteasy.vip.third.ThirdTask;
import com.takeiteasy.vip.weather.MeteoService;

import javax.jws.WebService;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

    private static int counter = 0;
    private static AtomicInteger atomicCounter;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        SecondTask secondTask = new SecondTask(scanner);
//        secondTask.run();
//        ThirdTask thirdTask = new ThirdTask(scanner);
//        thirdTask.run();

//        CurrencyConverter converter = new CurrencyConverter(scanner);
//        converter.run();

//        Library library = new Library(scanner);
//        library.run();
//        Bookkeeping bookkeeping = new Bookkeeping();
//        bookkeeping.run();

//        Finance finance = new Finance();
//        finance.run();

//        Cafe cafe = new Cafe(scanner);
//        cafe.run();

//        atomicCounter = new AtomicInteger();
//
//        Counter counter = new Counter();
//        counter.start();
//
//        Thread thread = new Thread(new CounterRunnable());
//        thread.start();

        MeteoService meteoService = new MeteoService(scanner);
        meteoService.run();
    }

    static synchronized void inc() {
        counter++;
    }

    static class Counter extends Thread {
        private static final int MAX_COUNT_NUMBER = 1_000_000;

        Counter() {
        }

        @Override
        public synchronized void start() {
            super.start();
            System.out.println("Thread start.");
        }

        void count() {
            for (int i = 0; i < MAX_COUNT_NUMBER; i++) {
                inc();
                atomicCounter.incrementAndGet();
            }

            System.out.println("Counter - " + counter);
            System.out.println("Atomic counter - " + atomicCounter.get());
        }

        @Override
        public void run() {
            count();
        }
    }

    static class CounterRunnable implements Runnable {
        private static final int MAX_COUNT_NUMBER = 1_000_000;

        CounterRunnable() {

        }

        void count() {
            for (int i = 0; i < MAX_COUNT_NUMBER; i++) {
                inc();
                atomicCounter.incrementAndGet();
            }

            System.out.println("Counter - " + counter);
            System.out.println("Atomic counter - " + atomicCounter.get());
        }

        @Override
        public void run() {
            count();
        }
    }
}
