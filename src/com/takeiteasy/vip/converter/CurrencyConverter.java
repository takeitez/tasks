package com.takeiteasy.vip.converter;

import java.util.Scanner;

public class CurrencyConverter {
    private static final String BANKS_DELIMITER = ", ";
    private static final String SALE = "sale";
    private static final String PURCHASE = "purchase";
    private final Scanner scanner;

    private Bank[] banks;

    public CurrencyConverter(Scanner scanner) {
        this.scanner = scanner;
        banks = createBanksArray();
    }

    private Bank[] createBanksArray() {
        Bank[] banks = new Bank[3];

        banks[0] = new Bank(
                "Privat",
                new Currency[] { new Currency("USD", 27.1f, 27.2f), new Currency("EUR", 32f, 32.1f), new Currency("GBP", 36.2f, 36.3f) }
        );
        banks[1] = new Bank(
                "Pumb",
                new Currency[] { new Currency("USD", 27.05f, 27.15f), new Currency("EUR", 31.95f, 32.05f), new Currency("GBP", 36.15f, 36.25f) }
        );
        banks[2] = new Bank(
                "Oshad",
                new Currency[] { new Currency("USD", 27.0f, 27.1f), new Currency("EUR", 31.9f, 32f), new Currency("GBP", 36.1f, 36.2f) }
        );

        return banks;
    }

    public void run() {
        System.out.println("Enter the amount of money in UAH you want to change: ");
        int amount = Integer.parseInt(scanner.nextLine());

        System.out.println("Enter name of bank rate of which will be used for conversion (one of " + getListOfBanks() + "): ");
        String bankName = scanner.nextLine();

        Bank bank = findBankByName(bankName);

        if (bank == null) {
            System.out.println("No bank with \"" + bankName + "\" name found.");
            return;
        }

        System.out.println("Enter the currency to convert (" + bank.getListOfSupportedCurrencies() + "): ");
        String currency = scanner.nextLine();

        System.out.println("Enter rate (sale or purchase): ");
        String rate = scanner.nextLine();

        // I know this is not best option to handle rate input but I'm tired =(.
        if (!rate.equalsIgnoreCase(SALE) && !rate.equalsIgnoreCase(PURCHASE)) {
            System.out.println("Can not resolve \"" + rate + "\" rate.");
            return;
        }

        // Still not best option.
        float conversionResult = bank.convert(amount, currency, rate.equalsIgnoreCase(SALE));

        if (conversionResult < 0) {
            System.out.println(currency.toUpperCase() + " is not supported by " + bank.getName() + " bank.");
            return;
        }

        System.out.println("Conversion result for \"" + bank.getName() + "\" UAH -> " + currency.toUpperCase() + " : " + conversionResult);
    }

    private String getListOfBanks() {
        StringBuilder sb = new StringBuilder();

        for (Bank bank : banks) {
            if (sb.length() > 0) {
                sb.append(BANKS_DELIMITER);
            }

            sb.append(bank.getName());
        }

        return sb.toString();
    }

    private Bank findBankByName(String name) {
        for (Bank bank : banks) {
            if (bank.getName().equalsIgnoreCase(name)) {
                return bank;
            }
        }

        return null;
    }
}
