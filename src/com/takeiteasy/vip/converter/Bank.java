package com.takeiteasy.vip.converter;

class Bank {
    private static final String CURRENCIES_DELIMITER = ", ";
    private final String name;
    private final Currency[] supportedCurrencies;

    Bank(String name, Currency[] supportedCurrencies) {
        this.name = name;
        this.supportedCurrencies = supportedCurrencies;
    }

    String getName() {
        return name;
    }

    String getListOfSupportedCurrencies() {
        StringBuilder sb = new StringBuilder();

        for (Currency currency : supportedCurrencies) {
            if (sb.length() > 0) {
                sb.append(CURRENCIES_DELIMITER);
            }

            sb.append(currency.getName());
        }

        return sb.toString();
    }

    float convert(int amount, String currencyName, boolean isSaleRate) {
        for (Currency currency : supportedCurrencies) {
            if (currency.getName().equalsIgnoreCase(currencyName)) {
                return amount / (isSaleRate ? currency.getSaleRate() : currency.getPurchaseRate());
            }
        }

        return -1;
    }
}
