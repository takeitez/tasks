package com.takeiteasy.vip.converter;

class Currency {
    private final String name;
    private final float purchaseRate;
    private final float saleRate;

    Currency(String name, float purchaseRate, float saleRate) {
        this.name = name;
        this.purchaseRate = purchaseRate;
        this.saleRate = saleRate;
    }

    String getName() {
        return name;
    }

    float getPurchaseRate() {
        return purchaseRate;
    }

    float getSaleRate() {
        return saleRate;
    }
}
