package com.takeiteasy.vip.third;

import java.util.Arrays;

class Book {
    String title;
    String[] authors;
    String publisher;
    int publishingYear;

    Book(String title, String[] authors, String publisher, int publishingYear) {
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.publishingYear = publishingYear;
    }

    boolean isInCoAuthorship(String authorName) {
        if (!hasMultipleAuthors())
            return false;

        for (String author : authors) {
            if (author.equals(authorName))
                return true;
        }

        return false;
    }

    private boolean hasMultipleAuthors() {
        return authors.length > 1;
    }

    void printAuthorsList() {
        System.out.println("Author" + (hasMultipleAuthors() ? "s" : "") + " of oldest book in the list " +
                (hasMultipleAuthors() ? "are " : "is ") + Arrays.toString(authors));
    }

    void printBookInfo() {
        System.out.println("Title: " + title
                + ", Authors: " + Arrays.toString(authors)
                + ", Publisher: " + publisher
                + ", Year of publishing: " + publishingYear);
    }

    boolean isWrittenBy(String authorName) {

        for (String author : authors) {
            if (author.equals(authorName)) {
                return true;
            }
        }

        return false;
    }

    boolean isOlderThan(Book book) {
        return this.publishingYear > book.publishingYear;
    }
}
