package com.takeiteasy.vip.third;

class Shelf {
    private Book[] books;

    Shelf() {
        this.books = createBooksArray();
    }

    private Book[] createBooksArray() {
        Book[] array = new Book[5];

        array[0] = new Book("Book0", new String[] {"Author0", "Author2"}, "Publisher0", 1992);
        array[1] = new Book("Book1", new String[] {"Author1"}, "Publisher1", 1996);
        array[2] = new Book("Book2", new String[] {"Author2"}, "Publisher2", 1997);
        array[3] = new Book("Book3", new String[] {"Author3"}, "Publisher3", 1991);
        array[4] = new Book("Book4", new String[] {"Author4", "Author0", "Author1"}, "Publisher4", 2001);

        return array;
    }

    void printBooksPublishedBefore(int publishingYear) {
        System.out.println("Books published before " + publishingYear + " year:");
        boolean isAnyFound = false;

        for (Book book : books) {
            if (book.publishingYear < publishingYear) {
                isAnyFound = true;
                book.printBookInfo();
            }
        }

        if (!isAnyFound)
            System.out.println("No books found.");
    }

    void printAuthorBooks(String authorName) {
        System.out.println("Books written by " + authorName + ":");
        boolean isAnyFound = false;

        for (Book book : books) {
            if (book.isWrittenBy(authorName)) {
                isAnyFound = true;
                System.out.println(book.title);
            }
        }

        if (!isAnyFound)
            System.out.println("No books found.");
    }

    void printOldestBook() {
        if (books.length == 0) {
            System.out.println("There is no books on the shelf.");
        }

        Book oldestBook = books[0];

        for (int i = 1; i < books.length; i++) {
            if (oldestBook.isOlderThan(books[i])) {
                oldestBook = books[i];
            }
        }

        oldestBook.printAuthorsList();
    }

     void printAuthorBooksWrittenInCoAuthorship(String authorName) {
        System.out.println("Books written by " + authorName + " in co-authorship with somebody:");
        boolean isAnyFound = false;

        for (Book book : books) {
            if (book.isInCoAuthorship(authorName)) {
                isAnyFound = true;
                book.printBookInfo();
            }
        }

        if (!isAnyFound)
            System.out.println("No books found.");
    }

    void printBooksWithAuthorsCountGreaterOrEqual(int authorsCount) {
        System.out.println("Books with authors number greater or equal " + authorsCount + ":");
        boolean isAnyFound = false;

        for (Book book : books) {
            if (book.authors.length >= authorsCount) {
                isAnyFound = true;
                book.printBookInfo();
            }
        }

        if (!isAnyFound)
            System.out.println("No books found.");
    }


}
