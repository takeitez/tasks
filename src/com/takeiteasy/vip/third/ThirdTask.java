package com.takeiteasy.vip.third;

import java.util.Arrays;
import java.util.Scanner;

public class ThirdTask {
    private final Scanner scanner;

    private Shelf shelf;

    public ThirdTask(Scanner scanner) {
        this.scanner = scanner;
        this.shelf = new Shelf();
    }

    public void run() {
        shelf.printOldestBook();
        System.out.println();
        String authorName = requestAuthorName();
        shelf.printAuthorBooks(authorName);
        System.out.println();
        shelf.printAuthorBooksWrittenInCoAuthorship(authorName);
        System.out.println();
        shelf.printBooksPublishedBefore(requestPublishingYear());
        System.out.println();
        shelf.printBooksWithAuthorsCountGreaterOrEqual(3);
    }

    private int requestPublishingYear() {
        System.out.println("Enter year of book publishing:");
        return Integer.valueOf(scanner.nextLine());
    }

    private String requestAuthorName() {
        System.out.println("Enter name of author:");
        return scanner.nextLine();
    }


}
